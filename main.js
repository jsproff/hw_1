
const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];
//var newIt = ITEA_COURSES.map(function(el){return el.length});

// var it = ITEA_COURSES.sort();
// it.forEach(function(el){
//     var ul = document.createElement('ul');
//     document.body.appendChild(ul);
//     var li = document.createElement('li');
//     li.innerText = el;
//     ul.appendChild(li);
// });

var form = document.querySelector('#find'),
    output = document.querySelector('#output');

form.addEventListener('submit', find);

function find(e) {
    e.preventDefault();

    var val = e.target.val.value;
    var rez = ITEA_COURSES.find(function(el) {
        return el.toLowerCase().indexOf(val.toLowerCase()) > -1;

    });
    output.innerText = rez;

}